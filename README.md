# Free Enterprise

An open world randomizer for Final Fantasy IV.

----

This is a placeholder repository for Free Enterprise, which is not yet open
source. When the source is released, it will be available here.

In the meantime, this project's issue tracker will be used to keep track of
known issues with Free Enterprise.